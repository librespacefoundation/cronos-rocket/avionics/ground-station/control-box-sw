import subprocess
import serial


class Transceiver:
    def __init__(self, device, baudRate):
        self.device = device
        self.baudRate = baudRate
        self.port = serial.Serial(device, baudRate)

    def transmit(self, data):
        # subprocess.run(["echo", f"{data}", ">", f"{self.device}"])
        self.port.write(data)
        return self

    def receive(self, size=1):
        """
        Parameters:
            size (int): Number of bytes to read. Default size is 1.
        """
        received_data = self.port.read(size=size)
        return received_data

    def getCurrentPortSetup(self):
        subprocess.run(["​stty", "-F", f"{self.device}"])
