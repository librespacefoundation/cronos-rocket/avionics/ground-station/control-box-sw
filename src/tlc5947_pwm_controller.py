import yaml
import board
import busio
import digitalio
import adafruit_tlc5947

with open('config.yml') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)


class PWMController:
    """
    Controls rgb leds which do not have a blue pin.
    """
    def __init__(self, dc_bits=16, auto_write=True):
        # Define pins connected to the TLC5947
        self.SCK = board.SCLK
        self.MOSI = board.MOSI
        self.LATCH = digitalio.DigitalInOut(board.D5)

        # TLC5947 parameters
        self.dc_bits = dc_bits  # ${dc_bits} duty-cycle property value
        self.auto_write = auto_write  # if false then manualy call tlc5947.write()

        # Initialize SPI bus.
        self.spi = busio.SPI(clock=self.SCK, MOSI=self.MOSI)

        # Initialize TLC5947
        self.tlc5947 = adafruit_tlc5947.TLC5947(self.spi, self.LATCH, auto_write=auto_write)

        self.leds = {}

    def setupLED(self, id_, redPin, greenPin):
        self.leds[id_] = {
            'red': self.tlc5947.create_pwm_out(redPin),
            'green': self.tlc5947.create_pwm_out(greenPin),
        }
        return self

    def setLED(self, id_, redValue, greenValue):
        self.validate(self.dc_bits, redValue, greenValue)

        self.leds[id_]['red'].duty_cycle = redValue
        self.leds[id_]['green'].duty_cycle = greenValue

        if not self.auto_write:
            self.tlc5947.write()

        return self

    def getLED(self, id_):
        return self.leds[id_]

    def getAllLEDS(self):
        return self.leds

    @staticmethod
    def validate(dc_bits, redValue, greenValue):
        if dc_bits == 16:
            LOW = config["TLC5947"]["duty_cycle"]["PWMOut"]["low"]
            HIGH = config["TLC5947"]["duty_cycle"]["PWMOut"]["high"]
        elif dc_bits == 12:
            LOW = config["TLC5947"]["duty_cycle"]["native"]["low"]
            HIGH = config["TLC5947"]["duty_cycle"]["native"]["high"]

        isValid = all([LOW <= value <= HIGH for value in [redValue, greenValue]])
        if not isValid:
            raise ValueError
