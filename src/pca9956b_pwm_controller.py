import logging
import time
import math
import yaml

logger = logging.getLogger(__name__)

with open('config.yml') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

adr_list = config['pca9956b']['address']

reg_list = config['pca9956b']['register']

mode1_bit_on_list = config['pca9956b']['MODE1Bits']['allow']
mode1_bit_off_list = config['pca9956b']['MODE1Bits']['disable']
                
mode2_bit_on_list = config['pca9956b']['MODE2Bits']['allow']
mode2_bit_off_list = config['pca9956b']['MODE2Bits']['disable']

led_bit_mode = config['pca9956b']['LEDOUTMode']

def read_pca9956b() :
    def read_PWMx(state, register, ldrx):
        register_status = PCA9956B().read_register(register)

        if (ldrx == led_bit_mode['PWM'] or (ldrx == led_bit_mode['PWM_GRPPWM'] and reg_mode2['DMBLNK'] == 'BLINKING')) : 
          state[register] = round(register_status / 256 * 100, 1)
        
        elif ldrx == led_bit_mode['PWM_GRPPWM'] :
          state[register] = round((register_status & 0xFC) / 256 * 100, 1) 
        
        elif ldrx == led_bit_mode['IS_ON'] :
          state[register] = 100
        
        else:
          state[register] = 0

    state = {}

    # ------------------
    # READ MODEx state
    # ------------------

    mode1_status = PCA9956B().read_register(register='MODE1')
    mode2_status = PCA9956B().read_register(register='MODE2')

    reg_mode1 = {}
    mode1_keys = list(mode1_bit_on_list.keys())

    for key in mode1_keys:
        reg_mode1[key] = 'ON' if mode1_status & mode1_bit_on_list[key] > 0  else 'OFF'

    reg_mode2 = {}
    reg_mode2['OVERTEMP'] = 'ON' if mode2_status & mode2_bit_on_list['OVERTEMP'] > 0 else 'OFF'
    reg_mode2['ERROR'] = 'ON' if mode2_status & mode2_bit_on_list['ERROR'] > 0 else 'OFF'
    reg_mode2['DMBLNK'] = 'DIMMING' if mode2_status & mode2_bit_on_list['DMBLNK'] > 0 else 'BLINKING'
    reg_mode2['CLRERR'] = 'ON' if mode2_status & mode2_bit_on_list['CLRERR'] > 0 else 'OFF'
    reg_mode2['OCH'] = 'ON' if mode2_status & mode2_bit_on_list['OCH'] > 0 else 'OFF'

    state['MODE1'] = reg_mode1
    state['MODE2'] = reg_mode2

    # ------------------
    # READ PWMx state
    # ------------------

    LDRX = {}
    for i in range(6):
        register = 'LEDOUT{}'.format(i)
        register_status = PCA9956B().read_register(register)
        
        ledout_x_bits = 'LEDOUT{}Bits'.format(i)
        ldr_list = list(config['pca9956b'][ledout_x_bits]['read'].keys())
        ldr_idx_list = [ldrx.split('LDR')[1] for ldrx in ldr_list]

        LDRX[ldr_list[0]] = register_status & 0x03 
        LDRX[ldr_list[1]] = (register_status & 0x0c) >> 2
        LDRX[ldr_list[2]] = (register_status & 0x30) >> 4
        LDRX[ldr_list[3]] = (register_status & 0xc0) >> 6

        for idx, name in zip(ldr_idx_list, ldr_list):
            ldrx = LDRX[name]
            register = 'PWM{}'.format(idx)

            read_PWMx(state, register, ldrx)

    # ------------------
    # READ GRPx state
    # ------------------

    state['GRPPWM'] = str(PCA9956B().read_register(register='GRPPWM'))
    state['GRPFREQ'] = str(PCA9956B().read_register(register='GRPFREQ')) 
   
    # ------------------
    # READ LDRx state
    # ------------------

    for i in range(6):
        reg_ledoutx = {}

        register = 'LEDOUT{}'.format(i)

        ledout_x_bits = 'LEDOUT{}Bits'.format(i)
        ldr_list = list(config['pca9956b'][ledout_x_bits]['read'].keys())

        for i in ldr_list:
            for key, value in led_bit_mode.items():
                if LDRX[i] == value:
                    reg_ledoutx[i] = key

        state[register] = reg_ledoutx

    # --------------------
    # READ SUBADRx state
    # --------------------

    state['SUBADR1'] = str(hex((PCA9956B().read_register(register='SUBADR1') & 0xFE) >> 1))
    state['SUBADR2'] = str(hex((PCA9956B().read_register(register='SUBADR2') & 0xFE) >> 1))
    state['SUBADR3'] = str(hex((PCA9956B().read_register(register='SUBADR3') & 0xFE) >> 1))

    # -----------------------
    # READ ALLCALLADR state
    # -----------------------

    state['ALLCALLADR'] = str(hex((PCA9956B().read_register(register='ALLCALLADR') & 0xFE) >> 1))

    return state


def software_reset(address=adr_list['PCA9956B_GENERALCALL'], busnum=adr_list['I2CBUS'], i2c=None, **kwargs):
    '''Sends a software reset (SWRST) command to all servo drivers on the bus.'''
    
    # Setup I2C interface for device 0x00 to talk to all of them.
    if i2c is None:
        import i2c_pkg.i2c as I2C
        i2c = I2C
    try:
        device = i2c.get_i2c_device(address, busnum, **kwargs) # General Call
        device.writeRaw8(0x06)
    except:
        return 1
    else:
        return 0

     
def ledout_clear():
    for i in range(6):
        register = 'LEDOUT{}'.format(i)

        ledout_x_bits = 'LEDOUT{}Bits'.format(i)
        ldr_list = list(config['pca9956b'][ledout_x_bits]['read'].keys())

        bits = [dict.fromkeys(ldr_list, 'IS_OFF')]

        try:
            PCA9956B().write_register(register, bits)
        except:
            return 1
        else:
            return 0


class PCA9956B(object):
    '''PCA9956B() PWM LED controller.'''

    def __init__(self, address=adr_list['PCA9956B_ADDRESS'], i2c=None, **kwargs):
        '''Initialize the PCA9956B.'''
        
        # Setup I2C interface for the device.
        if i2c is None:
            import i2c_pkg.i2c as I2C
            i2c = I2C
        
        self._device = i2c.get_i2c_device(address, **kwargs)
        #self._device.write8(PWM0,0x7D)
    
    def self_test(self):
        try:
            self.read_register(register='MODE1')
        except:
            return 1
        else:
            return 0
    
    def read_register(self, register) :
        return self._device.readU8(reg_list[register])
    
    def write_register(self, register, bits) :
        if register == 'MODE1':
            self.set_MODEx(register, bits, mode1_bit_on_list, mode1_bit_off_list)

        elif register == 'MODE2' :
            self.set_MODEx(register, bits, mode2_bit_on_list, mode2_bit_off_list)

        elif re.match(r'(PWM)([0-9]|1[0-9]|2[0-3])?$', register):
            self.set_PWMx(self, register, bits)

        elif (register == 'GRPPWM' or  register == 'GRPFREQ'):
            for key, value in bits[0].items():
                self.write8(register, int(value))

        elif re.match(r'(LEDOUT)[0-5]$', register):
            ret = 0
            reg_status = self.read_register(register)
            
            ledout_x_bits = '{}Bits'.format(register)
            ledx_bit_list = config['pca9956b'][ledout_x_bits]['write']

            for ibit in bits :
                bit_key = list(ibit)[0]
                bit_value = ibit[bit_key]

                self.set_LDRx(register=bit_key, mode=bit_value)

                index = bit_key[3:]

                bit_led = led_bit_mode[bit_value]

                bit_m1 = reg_status & ledx_bit_list['{}_W'.format(bit_key)]
                bit_m1 = '00000000'[:(8 - len(bin(bit_m1)[2:]))] + bin(bit_m1)[2:]

                if (index % 4) == '3':
                    bit_m2 = '00'[:(2 - len(bin(bit_led)[2:]))] + bin(bit_led)[2:] + '000000'
                elif (index % 4) == '2':
                    bit_m2 = '0000'[:(2 - len(bin(bit_led)[2:]))] + bin(bit_led)[2:] + '0000'
                elif (index % 4) == '1':
                    bit_m2 = '000000'[:(2 - len(bin(bit_led)[2:]))] + bin(bit_led)[2:] + '00'
                else:
                    bit_m2 = '00000000'[:(8 - len(bin(bit_led)[2:]))] + bin(bit_led)[2:]

                try:
                    reg_status = int(bit_m1, 2) ^ int(bit_m2, 2) 
                    self.write8(register, int(reg_status))
                except:
                    ret += 1

            return ret

    def set_MODEx(self, register, bits, modeX_bit_on_list, modeX_bit_off_list):
        ret = 0
        reg_status = self.read_register(register)

        for ibit in bits :
            try :
                bit = modeX_bit_on_list[ibit]
                reg_status = reg_status ^ bit
            except :
                bit = modeX_bit_off_list[ibit]
                reg_status = reg_status & bit
            finally:
                ret += self.write8(register, int(reg_status))

    def set_PWMx(self, register, bits):
        for key, value in bits[0].items():
            if key == 'PWM':
                pass

            elif key == 'GRPPWM':
                value = int(value) & 0xFC

            self.write8(register, int(value))

    def set_LDRx(self, register, mode):
        register_idx = register.split('LDR')[1]
        write_reg = 'PWM{}'.format(register_idx)

        if mode == 'IS_OFF':
            set_value = 0x00
        elif mode == 'IS_ON':
            set_value = 0xFF

        self.write8(write_reg, set_value)

    def write8(self, register, value):
        try:
            self._device.write8(reg_list[register], value)
        except:
            ret = 1
        else:
            ret = 0

        return ret
