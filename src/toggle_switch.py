import digitalio


class ToggleSwitch:
    def __init__(self, pin):
        self.pin = pin
        self.switch = digitalio.DigitalInOut(pin)
        self.switch.direction = digitalio.Direction.INPUT

    def read(self):
        return self.switch.value
