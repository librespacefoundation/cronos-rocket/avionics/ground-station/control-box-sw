import time
import yaml
import board
from  pca9956b_pwm_controller import pca9956b
from toggle_switch import ToggleSwitch
from uart_transceiver import Transceiver

with open('config.yml') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

SWITCHES = ["FILL", "VALVE_CHECK", "FIRE", "APPROACH", "ABORT"]

RED = {
    'R': 255,
    'G': 0,
    'B': 0,
}

GREEN = {
    'R': 0,
    'G': 255,
    'B': 0,
}

if __name__ == "__main__":
    transceiver = Transceiver("/dev/serial0", 9600)
    

    switch = ToggleSwitch(pin=board.D14)
    # switches = [ToggleSwitch(pin=board[config["SWITCHES"][pin]]) for pin in SWITCHES]

    # PCA9956B
    pwm = pca9956b.PCA9956B()

    state = pca9956b.read_pca9956b()
    print('PCA9956B state', state)
    
    ret = pca9956b.software_reset()
    print('Software reset', ret)
    
    ret = pca9956b.ledout_clear() 
    print('Ledout clear', ret)


    while True:
        on = switch.read()
        receivedData = transceiver.receive()

        if on:
            pwm.write_register(register='PWM3', bits=[{'PWM': GREEN['R']}])
            pwm.write_register(register='PWM2', bits=[{'PWM': GREEN['G']}])
            pwm.write_register(register='PWM1', bits=[{'PWM': GREEN['B']}])
            transceiver.transmit('THRUST')

        else:
            pwm.write_register(register='PWM3', bits=[{'PWM': RED['R']}])
            pwm.write_register(register='PWM2', bits=[{'PWM': RED['G']}])
            pwm.write_register(register='PWM1', bits=[{'PWM': RED['B']}])

        time.sleep(0.01)

        dataLeft = transceiver.port.inWaiting()
        receivedData += transceiver.receive(dataLeft)
        print('received data: ', receivedData)
