FROM balenalib/raspberrypi3-debian-python:latest

RUN mkdir -p /controlbox
WORKDIR /controlbox

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "src/main.py" ]
