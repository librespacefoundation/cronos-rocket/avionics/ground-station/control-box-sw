# :video_game: GroundStation-ControlBox
Use LXTerminal for the following steps:
	
```bash
conda create -n controlBox python=3.7.9
source activate controlBox
pip install -r requirements.txt
python src/main.py
```

Alternatively, 

```bash
docker build -t controlbox .
docker run -it --rm --name controlbox-container controlbox
```

